! ********************************************************************* !
! Forward modelling module (part of TERT package)
! Copyright (C) 2017  Erica Galetti
! 
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <http://www.gnu.org/licenses/>.
! ********************************************************************* !


MODULE forward

USE input_files, ONLY: modelling_method,forward_file
USE acquisition, ONLY: number_of_measurements
USE string_conversion

CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE forward_modelling(number_of_cells,voronoi_model,modelled_data,info)

IMPLICIT NONE
INTEGER, INTENT(IN)                                                             :: number_of_cells
DOUBLE PRECISION, DIMENSION(:,:), INTENT(IN)                                    :: voronoi_model
DOUBLE PRECISION, DIMENSION(number_of_measurements), INTENT(OUT)                :: modelled_data
INTEGER, INTENT(OUT)                                                            :: info

IF (modelling_method.EQ.2) THEN
        
        CALL forward_with_fw2_5d(number_of_cells,voronoi_model(1:number_of_cells,[1,3,4]),modelled_data,info)
        
ELSE
        
        WRITE(*,*)
        WRITE(*,*)'********************************************'
        WRITE(*,*)'ERROR in subroutine forward_modelling'
        WRITE(*,*)'--------------------------------------------'
        WRITE(*,*)'Modelling method ',TRIM(int2str(modelling_method)),' does not exist!'
        WRITE(*,*)'Check input file mksamples.in'
        WRITE(*,*)'TERMINATING PROGRAM!!'
        WRITE(*,*)'********************************************'
        WRITE(*,*)
        STOP
        
END IF

END SUBROUTINE

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE forward_with_fw2_5d(number_of_cells,voronoi_model,resistance_data,info)

USE modelling_2_5D
USE voronoi_operations
IMPLICIT NONE
INTEGER, INTENT(IN)                                     :: number_of_cells
DOUBLE PRECISION, DIMENSION(:,:), INTENT(IN)            :: voronoi_model
DOUBLE PRECISION, DIMENSION(:), INTENT(OUT)             :: resistance_data
INTEGER, INTENT(OUT)                                    :: info
INTERFACE
        SUBROUTINE create_resistivity_grid2D(ncell,voronoi_model)
                INTEGER, INTENT(IN)                                     :: ncell
                DOUBLE PRECISION, DIMENSION(ncell,3), INTENT(IN)        :: voronoi_model
        END SUBROUTINE
END INTERFACE

IF (.NOT.ALLOCATED(cell_areas)) THEN
        
        ! Define input file name for forward modelling
        fw2_5d_file=forward_file
        
        ! Initialise the modelling grid and various modelling parameters
        CALL initialize_fw2_5d()
        
END IF

! Create 2D resistivity model
CALL create_resistivity_grid2D(number_of_cells,voronoi_model)

! Run forward modelling
CALL run_fw2_5d(resistance_data,info)

END SUBROUTINE

END MODULE